## # What do you want to address?
(This step is required; examples are shown below)

- [ ] Bug
- [ ] Feature
- [ ] Suggestion

## # Describe your matter briefly
(This step is required)


##### What did you expect? (Useful when addressing bugs)
---
_(This step is optional)_


##### Some additional details (Useful, when we are trying to reproduce a bug)
---
_(This step is optional; an example is shown below)_

* The version of **Gitea** you are using: 
* The version of **GitNex** you are using: 
* Phone **OS** version and model: 
* The type of certificate you are using (self-signed, signed): 
* How you used to log in (via password or token): 


##### We would appreciate some screenshots or stacktrace's, but this is also not required.
---
_(Screenshots and stacktrace's can go here)_

#### Thank you for your time.