- New: Edit issue
- New: Tabs for open and closed issues
- Enhancement: German translation
- Fixed: Layout fixes across the app

And many more, check the release notes.
https://gitea.com/gitnex/GitNex/releases
